package reverser;

import java.util.*;

public final class BinTree {
	private class node {
		node (int i) {
			iData = i;
		}
		node left;
		node right;
		int iData;
	}
	BinTree() {
		top = null;
		pCount = 0;
	}
	void addAt(int i, node root) {
		if (root == null) {
			top = new node (i);
		} 
		else if (root.iData == i){
			return;			
		}
		else
		if (root.iData > i)
		{
			if (root.left == null) {
				root.left = new node(i);
			}
			else { 
				addAt(i, root.left);
			}
		} else {
			if (root.right == null){
				root.right = new node(i);
			}
			else {
				addAt(i, root.right);
			}
		}
	}
	void add(int i) {
		addAt(i, top);		
	}
	StringBuffer toStringBuf(StringBuffer s, node n){
 		if (n==null) {
			return s;
		}
		else {
			if (++pCount > 40) {
				s.append("\n");
				pCount = 0;
			}
			toStringBuf(s, n.left);
			s = s.append( " " + n.iData);
			toStringBuf(s, n.right);			
		}
 		return s;
	}
	private int pCount;
	
	@Override
	public String toString() {
	StringBuffer s = new StringBuffer("bintree ");
	toStringBuf(s, top);
	return new String(s);
	}
	
	private node top;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BinTree b = new BinTree();
		String s = b.toString();
		System.out.println(s);
		Random randomGenerator = new Random();
	    for (int idx = 1; idx <= 100; ++idx){
	      int x = randomGenerator.nextInt(100);
			b.add(x);
		}
		s = b.toString();
		System.out.println(s);
	}

}
